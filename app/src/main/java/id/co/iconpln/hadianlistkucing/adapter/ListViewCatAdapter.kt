package id.co.iconpln.hadianlistkucing.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.iconpln.hadianlistkucing.R
import id.co.iconpln.hadianlistkucing.model.Cat

class ListViewCatAdapter(private val context: Context, private val listCat: ArrayList<Cat>) :
    BaseAdapter() {
    @SuppressLint("ViewHolder")
    override fun getView(index: Int, view: View?, viewGroup: ViewGroup): View {
        val viewLayout =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.item_list_cat, viewGroup, false)

        val viewHolder = ViewHolder(viewLayout)
        val cat = getItem(index) as Cat
        viewHolder.bind(context, cat)

        return viewLayout
    }

    override fun getItem(index: Int): Any {
        return listCat[index]
    }

    override fun getItemId(index: Int): Long {
        return index.toLong()
    }

    override fun getCount(): Int {
        return listCat.size
    }

    private inner class ViewHolder(view: View) {
        private val tvCatType: TextView = view.findViewById(R.id.tv_cat_type)
        private val tvCatDesc: TextView = view.findViewById(R.id.tv_cat_desc)
        private val ivCatImage: ImageView = view.findViewById(R.id.iv_cat_image)

        fun bind(context: Context, cat: Cat) {
            tvCatType.text = cat.type
            tvCatDesc.text = cat.desc

            // Load Image with Glide Library
            Glide.with(context)
                .load(cat.image)
                .apply(
                    RequestOptions()
                        .centerCrop()
                        .placeholder(R.drawable.img_cat_silhouette_primary)
                        .error(R.drawable.img_cat_silhouette_error)
                )
                .into(ivCatImage)
        }
    }
}