package id.co.iconpln.hadianlistkucing.utils

import android.content.Context
import android.text.TextUtils
import android.view.View
import androidx.appcompat.app.ActionBar
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import id.co.iconpln.hadianlistkucing.R

fun String.isEmailFormat(): Boolean {
    return !TextUtils.isEmpty(this) && android.util.Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

fun String.isValidPasswordLength(): Boolean {
    return this.length >= 7
}

fun String.editTextError(view: TextInputEditText): Int {
    view.error = this
    view.requestFocus()
    return 1
}

fun String.showSnackbarError(context: Context, view: View) {
    val snackbar = Snackbar.make(
        view,
        this, Snackbar.LENGTH_LONG
    )
    snackbar.setBackgroundTint(
        ContextCompat.getColor(
            context,
            R.color.design_default_color_error
        )
    )
    snackbar.show()
}

fun String.showSnackbarAlert(context: Context, view: View) {
    val snackbar = Snackbar.make(
        view,
        this, Snackbar.LENGTH_LONG
    )
    snackbar.setBackgroundTint(
        ContextCompat.getColor(
            context,
            R.color.button_default
        )
    )
    snackbar.show()
}

fun String.setActionBarTitle(actionBar: ActionBar?) {
    actionBar?.title = this
}