package id.co.iconpln.hadianlistkucing

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import kotlin.concurrent.schedule

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        timerAction()
    }

    private fun timerAction() {
        Timer("splash", false).schedule(1000) {
            doSplash()
        }
    }

    private fun doSplash() {
        val loginActivityClass = Intent(this, LoginActivity::class.java)
        startActivity(loginActivityClass)
        finish()
    }

}
