package id.co.iconpln.hadianlistkucing.model

object CatsData {
    val listDataCat: ArrayList<Cat>
        get() {
            // create empty list
            val list = ArrayList<Cat>()
            // add data cat to list
            for (data in dataCats) {
                val cat = Cat()
                cat.type = data[0]
                cat.desc = data[1]
                cat.image = data[2]

                list.add(cat)
            }
            return list
        }

    private val dataCats = arrayOf(
        arrayOf(
            "British Shorthair",
            "Kucing ini dengan mudah ditemukan di jalan-jalan di Inggris. Akan tetapi, kucing ini memiliki sejarah yang cukup menyedihkan. Ketika perang dunia kedua, banyak sekali jumlah kucing ini yang mati. Untungnya, dengan bantuan jenis kucing lain, British shorhair akhirnya kembali berkembang biak lagi. British Shorthair mendapatkan lonjakan popularitas pada tahun 1871 akibat adanya pertunjukkan kucing di Crystal Palace, London. Namun, tidak lama berselang yaitu pada tahun 1890an, jumlah British Shorthair sangat berkurang, bahkan bisa dibilang keberadaannya terancam punah.\n" +
                    "\n" +
                    "Hal tersebut disebabkan dua faktor utama. Pertama, kala itu kucing berbulu panjang seperti Persia dan Turkish Angora banyak diimpor ke Inggris sehingga pengembangbiak kucing ini berkurang.\n" +
                    "\n" +
                    "Faktor kedua terjadi akibat tragedi Perang Dunia I dan II. Konflik tersebut turut berperan dalam berkurangnya usaha dalam mengembangbiakkan kucing ini.\n" +
                    "\n" +
                    "Untuk mengurangi penurunan jumlah British Shorthair, para pengembangbiak mencoba menyilangkan kucing ini dengan kucing longhair lain seperti Persia. Campuran genetik ini lalu menghasilkan varian longhair dari British Shorthair.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-20-british-shorthair.jpg"
        ),
        arrayOf(
            "Abyssinian",
            "Kucing Abyssinian merupakan kucing domestik berbulu pendek, yang dinamai dari kota Abyssinia, Ethiopia. Awalnya mereka berasal dari Ethiopia, namun studi menunjukkan mereka berasal dari Mesir, dan mereka merupakan salah satu kucing dari Pharaoh. Kucing ini diketahui dipopulerkan pada tahun 1871, memenangkan juara ketiga pada Crystal Palace Cat Show.\n" +
                    "\n" +
                    "Ciri khas yang dikenal selain bulunya yang cenderung tipis dan telinganya yang terlihat besar adalah kucing ini merupakan salah satu kucing yang cerdas. Abyssinian cenderung sangat aktif, suka bermain, dan bahkan bisa dilatih untuk melakukan cat tricks sehingga bukan hal aneh apabila kamu berminat untuk mengajaknya jalan-jalan menggunakan tali di badannya layaknya seekor anjing. Mungkin pada awalnya mereka cenderung waspada dan pemalu terhadap orang yang tidak dia kenal. Akan tetapi, apabila kucing ini sudah percaya dengan orang sekitar dan pemiliknya, kucing ini merupakan kucing keluarga yang sangat menyenangkan karena sifat alamiahnya yang suka bermain dan penyayang terhadap sekitarnya.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-1-abyssinian.jpg"
        ),
        arrayOf(
            "European Shorthair",
            "Kucing ini termasuk kucing yang ‘simple’ dan ‘harmonis’. Dia memiliki bulu pendek yang rapih dan halus, dengan tidak memiliki penekanan pada bagian-bagian khusus yang lebih spesial daripada kucing lainnya. Postur tubuh kucing ini juga tergolong seimbang dengan tubuh yang lumayan berotot. Temperamen kucing ini sulit ditentukan persisnya karena kucing ini merupakan campuran dari beberapa jenis kucing. Namun meski begitu, kucing ini merupakan kucing keluarga dan kucign rumah yang sangat biak. Sebelumnya kucing ini dipelihara petani untuk membantu membasmi hama tikus karena sifat alamiah berburunya yang baik dan relatif cerdas.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-35-european-shorthair.jpg"
        ),
        arrayOf(
            "Aegean",
            "Aegean merupakan satu-satunya jenis kucing alamiah yang berasal dari Yunani. Maksudnya, kucing ini berkembang tanpa adanya interferensi manusia sama sekali. Mereka mendapatkan namanya dari Laut Aegean. Meski sudah merupakan kucing domestik, Aegean baru mendapatkan pengakuan pada tahun 1990.\n" +
                    "\n" +
                    "Aegean memiliki ciri khas bulu yang pendek dan tubuh yang cenderung berotot. Pasti sekilas kamu berpikir “wah, seperti kucing kampung ya” dan itu tidak salah juga. Kucing ini terdapat banyak di tempat asalnya itu sendiri yaitu Yunani, namun tidak begitu banyak tersebar diluar itu. Aegean merupakan salah satu jenis kucing tertua yang alamiah, bukan melalui persilangan antar ras lainnya yang menjadikan dia terbebas dari kelainan genetik.Tidak banyak yang memelihara dia diluar negara asalnya. Akan tetapi di Yunani itu sendiri banyak yang memelihara Aegean dan beberapa masih hidup liar juga di jalan. Kucing Aegean liar bertahan hidup melalui kemampuan alamiahnya dalam berburu maupun diberi makan oleh nelayan setempat. Meski terlihat independen, kucing Aegean sangatlah menyukai interaksi dengan manusia dan bahkan anak kecil.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-2-aegean.jpg"
        ),
        arrayOf(
            "Exotic Shorthair",
            "Kucing ini banyak mengadopsi penampilan kucing Persian dari wajah dan ciri khas bagian tubuhnya. Hanya saja sesuai kelihatannya, bulu kucing ini tidak selebat kucing Persian dan memiliki corak warna bulu yang relatif berbeda secara umum. Meskipun kucing ini cenderung kalem, tapi dia lebih aktif dan suka bermain dibandingkan kucing Persian, karena memiliki sifat bawaan dari American shorthair. Kucing ini juga sangat penyayang dan seringkali duduk ke pangkuan pemiliknya.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-36-exotic-shorthair.jpg"
        ),
        arrayOf(
            "FoldEx",
            "Foldex sekilas memiliki kesamaan bentuk tubuh dengan Exotic shorthair, badan yang bulat namun dengan struktur tulang yang kuat, dan wajah yang bulat dan menggemaskan.Ciri khas yang paling menonjol adalah telinganya yang terlipat, dan kaki-kakinya yang pendek layaknya kucing Scottish Fold. Kucing ini dikenal dengan sifatnya yang aktif, penasaran, dan bersahabat. Dia tidak ragu-ragu untuk mendekati orang asing dan sangat suka untuk dielus-elus.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-37-foldex.jpg"
        ),
        arrayOf(
            "Burmilla",
            "Burmilla merupakan hasil dari persilangan ‘tidak sengaja’ antara kucing Burmese dan Persia Chinchilla, jadilah nama gabungan dari dua ras tersebut. Kucing Burmese dikembangbiakkan pertama kali pada tahun 1981, dan mendapatkan pengakuan pada tahun 1987.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-22-burmilla.jpg"
        ),
        arrayOf(
            "Cymric",
            "Cymric memiliki ciri khas badannya yang berotot, agak chubby, dan yang spesial adalah bulunya yang panjang namun lebat dan padat, tersebar rata di tubuhnya menambahkan kesan ‘bundar’ si kucing. Ohya, kalau kamu lihat, ekornya juga tergolong sangat pendek. Cymric merupakan jenis kucing ‘pertunjukkan’ karena kesan manis elegan kucing ini sering menonjol di sirkus kucing. Dia sering menjadi kucing pertunjukkan karena Cymric tergolong cerdas dan mudah dilatih. Meskipun ekornya pendek, dia mampu melompat dengan lincah meski tanpa beban penyeimbang. Ya, seperti kedengarannya, Cymric sangat ideal sebagai kucing keluarga.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-29-cymric-cat.jpg"
        ),
        arrayOf(
            "Desert Lynx",
            "Kucing ini sekilas mirip hewan liar bernama Lynx, dan memiliki warna umumnya agak putih abu, meski kadang kadang berwarna abu abu dan corak garis-garis. Ciri fisik yang dapat diperhatikan adalah ekornya yang pendek dan kaki belakang cenderung lebih panjang. Kucing ini cocok sebagai kucing peliharaan seperti umumnya, namun perlu diperhatikan bahwa kucing ini memiliki sifat natural alamiah selalu on alert alias waspada dan relatif pintar layaknya predator. Desert Lynx masuk satu grup dengan kategori kucing Lynx lainyna seperti Highland Lynx, Mohave Bobs, dan Alpine Lynx.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-30-desert-lynx.jpg"
        ),
        arrayOf(
            "Dragon Li",
            "Kucing ini memiliki ciri khas corak tabby yaitu corak yang seperti loreng, umumnya warna hitam dan abu-abu. Li memiliki telinga yang menonjol dan tajam, serta tubuhnya relatif berotot. Meskipun banyak kucing yang sekilas mirip dengan Li ini, mereka tidak mudah ditemukan diluar China. Kucing ini memiliki sifat cerdas, aktif, dan bersahabat dengan manusia. Namun jangan salah, mereka merupakan pemburu yang sangat hebat, tidak hanya berburu tikus besar. Konon, mereka juga pernah dilatih bisa untuk mengambil koran dan mengantarkannya ke majikannya.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-33-dragon-li.jpg"
        ),
        arrayOf(
            "Japanese Bobtail",
            "Japanese Bobtail sangat senang bermain dan juga cerdas. Mereka interaktif dengan pemiliknya, dan bisa dilatih berbagai macam trik dan bisa diajak jalan jalan dengan ikatan di tubuhnya. Mereka kucing yang fleksibel ketika diabwa jalan-jalan, namun agak keras kepala soal hal yang mana yang ‘boleh’ atau yang mana yang ‘dilarang’. Disarankan untuk memelihara kucing atau anjing lagi sebagai temannya karena kucing ini ‘harus punya’ teman interaksi.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-42-japanese-bobtail.jpg"
        ),
        arrayOf(
            "Chantilly-Tiffanny",
            "Kucing coklat-keemasan ini merupakan hasil dari persilangan antara kucing Malaya dan kucing Burmese. Awalnya, kucing ini disangka punah pada tahun 1960an, tapi akhirnya ditemukan lagi dan dilanjutkan program perkembangbiakannya pada tahun 1970 di Amerika.\n" +
                    "\n" +
                    "Ciri khas yang menonjol adalah bulunya yang tebal, mulus seperti sutra, dan lembut. Umumnya Chantilly datang dengan warna coklat dan mata yang berwarna kuning dan ketika dewasa menjadi agak keemasan. Namun ada jenis Chantilly juga yang diterima dengan warna coklat, biru, merah gelap, dan juga warna merah kecoklatan. Karena bulunya yang tebal, grooming rambut kucing ini agak sedikit repot, terutama karena kucing ini juga suka menarik bulunya sendiri hingga bisa terlihat agak botak. Jadi agak lebih diperhatikan ya, karena kucing ini lebih rentan hairball.\n" +
                    "\n" +
                    "Seperti kelihatannya, kucing ini merupakan kucing yang baik untuk keluarga karena sifat sosialnya dan dia sangat senang berinteraksi dengan pemiliknya menggunakan meow dia yang lebut. Dia mungkin agak waspada dengan orang asing, tapi seiring waktu dia akan lebih jinak dan bersahabat.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-24-chantilly.jpg"
        ),
        arrayOf(
            "Lambkin",
            "Lambkin, atau kadang-kadang direferensikan sebagai Nanus Rex, merupakan salah satu jenis kucing yang langka, karena mereka juga masih tergolong ras baru. Meskipunt tergolong baru, kucing ini dikembangbiakkan diantara tahun 1987 dan 1991 oleh Terri Harris. Terri Harris menggabungkan genetik dari Selkirk Rex dan Munchkin. Persilangan dua ras tersebut adalah dengan tujuan untuk menciptakan ras kucing baru yang pendek dan memiliki fitur khas bulu dari Selkirk Rex.\n" +
                    "\n" +
                    "Sesuai hasil persilangannya, Lambkin memiliki kaki-tangan yang pendek, dengan bulu yang bergelombang dan keriting seperti Selkirk Rex. Bulu dari Lambkin bisa memiliki varian pendek ataupun panjang, tergantung genetik mana yang dominan dari orang tuanya. Lambkin juga memiliki sifat campuran dari kedua orang tuanya, yaitu sifat yang sangat suka bermain, insting alamiah dalam berburu, dan kecenderungan untuk menjadi kucing yang senang dipangku oleh pemiliknya.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-49-lambkin.jpg"
        ),
        arrayOf(
            "Nebelung",
            "Nebelung merupakan bahasa Jerman yang berarti “kabut” dikarenakan kucing ini memiliki warna bulu perak kebiruan. Nebelung seringkali disebut sebagai kucing Russian Blue dengan varian bulu yang panjang. Jenis kucing ini dikembangkan oleh Cora Cobb, yang terinspirasi dari salah satu anak kucing berwarna kebiruan. Dari sinilah bagaimana keturunan hingga Nebelung dikembangkan dan berhasil pada tahun 1986.\n" +
                    "\n" +
                    "Nebelung memiliki panjang yang mengagumkan dalam proporsinya terhadap wajah, leher, tubuh, dan bulunya. Dia memiliki mata berwarna hijau atau kuning-kehijauan. Warnanya solid dengan kecenderungan berwarna perak kebiruan. Secara keseluruhan, dia memiliki banyak kemiripan dengan Russian Blue, hanya dengan varian bulu yang lebih panjang dan tebal. Nebelung merupakan salah satu kucing tercerdan dan ekspektasi umur yang tinggi, hingga 16 tahun lebih.\n" +
                    "\n" +
                    "Nebelung memiliki reputasi sebagai kucing tenang dan lembut. Dia suka bermain dengan orang yang dia kenal dan cenderung vokal terhadap apa yang mereka rasakan. Hanya saja, mereka agak pemalu terhadap orang baru, dan cenderung mengamati sekitarnya sebelum dia memutuskan untuk ‘ikut campur’ atau tidak.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-60-nebelung.jpg"
        ),
        arrayOf(
            "Pixie Bob",
            "Pixie Bob berasal dari Amerika Serikat dan berkembang secara alamiah, bukan hasil persilangan maupun eksperimental. Meski memiliki sebutan ‘bob’ di belakangnya, Pixie tidak memiliki genetik dengan jenis kucing Bob lainnya. Pixie Bob sudah dianggap sebagai kucing domestik murni untuk sebagai kucing peliharaan.\n" +
                    "\n" +
                    "Pixie Bob memiliki penampilan sekilas seperti kucing liar, meski tidak memiliki genetik yang bersangkutan. Mereka memiliki warna agak abu-abu dengan corak belang belang. Pixie Bob lahir dengan mata berwarna kebiruan, yang nantinya berubah menjadi hijau atau keemasan. Pixie Bob merupakan kucing yang aktif, suka bermain, dan cenderung lebih berani serta penasaran terhadap hal yang baru. Karena kecerdasannya, Pixie Bob juga bisa dilatih untuk melakukan berbagai hal, salah satunya ada mengajaknya jalan-jalan keluar.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-70-pixie-bob.jpg"
        ),
        arrayOf(
            "Ragamuffin",
            "Kalau kamu berpikir jika Raggamuffin itu memiliki ikatan dengan Ragdoll, kamu benar. Ragamuffin merupakan hasil persilangan antara kucing Ragdoll dengan Persian, Himalayan, dan kucing longhair domestik lainnya. Hasil persilangan itu memberi perubahan dari segi penampilan yang memberi perbedaan dengan kucign Ragdoll.\n" +
                    "\n" +
                    "Perbedaan yang terlihat dari Ragdolls dan Ragamuffin dari segi bulu adalah, Ragdoll memiliki corak warna yang ‘mencolok’, dalam artian di ujung ujung tangan, kaki, dan wajah biasanya memiliki warna lebih gelap dibanding bagian tubuhnya. Diluar itu, tidak terlalu banyak perbedaan yang signifikan. Justru mereka memiliki kesamaan yang sangat baik. Yaitu, mereka memiliki sifat dan temperamen yang sama. Berarti, mereka merupakan kucing keluarga dan kucing yang sangat baik untuk mendampingi anak kecil.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-74-ragamuffin.jpg"
        ),
        arrayOf(
            "Singapura",
            "Salah satu jenis kucing domestik alami, Singapura dikatakan berasal dari jalanan di Singapura itu sendiri, dan dikembangkan pada tahun 1970an. Singapura juga kadang disebut Kucinta atau kucing got.\n" +
                    "\n" +
                    "Singapura memiliki ciri khas tubuhnya yang kecil, bulu yang pendek, serta warna keseluruhan yang cenderung solid dan gradasi. Matanya juga terlihat besar dibanding kepalanya. Meskipun kucing ini kecil, mereka sangat aktif dan selalu mencari perhatian. Kadang-kadang, saking ‘rese’nya kucing ini dia bisa dipanggil sebagai hama rumahan. Kucing Singapura sangat bersahabat dengan orang yang dia kenal dan hewan lainnya apabila dikenalkan sejak kecil. Namun, dia agak sedikit penakut dan waspada terhadap orang asing yang belum dia kenal. Mereka memiliki kecenderungan tidak nyaman dengan suara yang bising, jadi perhatikan ya apabila keluargamu memiliki anak kecil yang suka berteriak.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-86-singapura.jpg"
        ),
        arrayOf(
            "Norwegian Forest",
            "Norwegian Forest memiliki sangat banyak kemiripan dengan Maine Coon, diantaranya adalah bulunya yang sekilas terlihat sama-sama panjang dan lebat serta tubuhnya juga besar. Meskipun begitu, Maine Coon masih memegang predikat sebagai jenis kucing yang terbesar dan dari segi bulu juga Norwegian Forest agak sedikit lebih pendek. Mereka memiliki kesamaan sifat dengan Maine Coon, yaitu sama-sama lembut, penyayang, dan cerdas. Hanya saja, Norwegian Forest agak lebih ‘independen’ dan kadang-kadang suka malas berinteraksi atau punya dunianya sendiri. Namun, bukan berarti Norwegian Forest bukan kucing rumahan yang baik, ya! Mereka hanya sesekali perlu untuk diajak main keluar.",
            "https://meowcitizen.com/wp-content/uploads/2018/09/meowcitizen_jenis-kucing-lengkap-61-norwegian-forest.jpg"
        )
    )
}