package id.co.iconpln.hadianlistkucing.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Cat(
    var type: String = "",
    var desc: String = "",
    var image: String = ""
) : Parcelable