package id.co.iconpln.hadianlistkucing

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.drawable.BitmapDrawable
import android.os.Build
import android.os.Bundle
import android.os.Environment
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import id.co.iconpln.hadianlistkucing.model.Cat
import id.co.iconpln.hadianlistkucing.utils.setActionBarTitle
import id.co.iconpln.hadianlistkucing.utils.showSnackbarAlert
import id.co.iconpln.hadianlistkucing.utils.showSnackbarError
import kotlinx.android.synthetic.main.activity_cat_detail.*
import java.io.File
import java.io.FileOutputStream


@Suppress("DEPRECATION", "NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
class CatDetailActivity : AppCompatActivity() {

    companion object {
        const val EXTRA_CAT = "extra_cat"
        private const val STORAGE_PERMISSION_CODE = 0
    }

    private lateinit var cat: Cat

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cat_detail)

        getString(R.string.detail_text).setActionBarTitle(supportActionBar)
        enableBackButton()
        checkExtras()
        setClickListener()
    }

    private fun setClickListener() {
        iv_cat_det_image.setOnLongClickListener { showDownload() }
        btn_det_cat_share.setOnClickListener { shareData() }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            STORAGE_PERMISSION_CODE -> {
                if (grantResults.isNotEmpty() && permissions[0] == Manifest.permission.WRITE_EXTERNAL_STORAGE) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        saveImage()
                    } else {
                        getString(R.string.request_storage_access_text).showSnackbarError(
                            this,
                            cat_detail_layout
                        )
                    }
                }
            }
        }
    }

    private fun showDownload(): Boolean {
        val builder = AlertDialog.Builder(this)
        builder.setMessage(getString(R.string.save_to_gallery_question_text))
        builder.setPositiveButton(android.R.string.yes) { _, _ ->
            saveImage()
        }
        builder.setNegativeButton(android.R.string.no) { _, _ ->
            return@setNegativeButton
        }
        builder.show()

        return true
    }

    private fun saveImage() {
        try {
            // Save Image to Destination Folder
            val draw = iv_cat_det_image.drawable as BitmapDrawable
            val bitmap = draw.bitmap

            val sdCard: File = Environment.getExternalStorageDirectory()
            val dir = File(sdCard.absolutePath.toString() + "/Pictures")
            dir.mkdirs()
            val fileName =
                String.format("${cat.type}_%d.jpg", System.currentTimeMillis())
            val outFile = File(dir, fileName)
            val outStream = FileOutputStream(outFile)
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream)
            outStream.flush()
            outStream.close()
            getString(R.string.saving_picture_success_text).showSnackbarAlert(
                this,
                cat_detail_layout
            )
        } catch (e: Exception) {
            askStoragePermit()
        }
    }

    private fun askStoragePermit() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            requestPermissions(
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                STORAGE_PERMISSION_CODE
            )
        } else {
            getString(R.string.request_storage_access_text).showSnackbarError(
                this,
                cat_detail_layout
            )
        }
    }

    private fun enableBackButton() {
        val actionBar = supportActionBar
        actionBar!!.setDisplayHomeAsUpEnabled(true)
    }


    private fun checkExtras() {
        cat = intent.getParcelableExtra(EXTRA_CAT)
        renderData(cat)
    }

    private fun shareData() {
        // pass data from cat detail
        var shareBody = this.cat.type + "\n\n"
        shareBody += this.cat.desc + "\n\n"
        shareBody += this.cat.image + "\n"

        val sharingIntent = Intent(Intent.ACTION_SEND)
        sharingIntent.type = "text/plain"
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "MeowPedia")
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody)
        startActivity(
            Intent.createChooser(sharingIntent, "SHARE INFO")
        )
    }

    private fun renderData(cat: Cat) {
        tv_cat_det_type.text = cat.type
        tv_cat_det_desc.text = cat.desc

        // Load Image with Glide Library
        Glide.with(this)
            .load(cat.image)
            .apply(
                RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.img_cat_silhouette_primary)
                    .error(R.drawable.img_cat_silhouette_error)
            )
            .into(iv_cat_det_image)
    }
}
