package id.co.iconpln.hadianlistkucing

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.widget.ListView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import id.co.iconpln.hadianlistkucing.model.Cat
import id.co.iconpln.hadianlistkucing.model.CatsData
import id.co.iconpln.hadianlistkucing.adapter.ListViewCatAdapter
import id.co.iconpln.hadianlistkucing.utils.setActionBarTitle
import id.co.iconpln.hadianlistkucing.utils.showSnackbarAlert
import id.co.iconpln.hadianlistkucing.viewModel.MainViewModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.dialog_about.*


class MainActivity : AppCompatActivity() {

    private val listCat: ListView get() = lv_list_cat
    private val list: ArrayList<Cat> = arrayListOf()
    private lateinit var mainViewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        getString(R.string.app_name).setActionBarTitle(supportActionBar)
        setActionBarHomeIcon()
        initViewModel()
        loadListBaseAdapter(this)
        setListItemClickListener(listCat)
    }

    private fun setActionBarHomeIcon() {
        supportActionBar?.setHomeAsUpIndicator(R.drawable.img_cat_silhouette_white_24dp)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun initViewModel() {
        mainViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)
    }

    private fun loadListBaseAdapter(context: Context) {
        list.clear()
        when (mainViewModel.isAsc) {
            null -> list.addAll(CatsData.listDataCat)
            true -> list.addAll(CatsData.listDataCat.sortedWith(compareBy { it.type }))
            false -> list.addAll(CatsData.listDataCat.sortedByDescending { it.type })
        }
        val baseAdapter = ListViewCatAdapter(context, list)
        listCat.adapter = baseAdapter
    }

    private fun setListItemClickListener(listView: ListView) {
        listView.setOnItemClickListener { _, _, position, _ ->
            val cat = list[position]
            val catDetailIntent = Intent(this, CatDetailActivity::class.java)
            catDetailIntent.putExtra(CatDetailActivity.EXTRA_CAT, cat)
            startActivity(catDetailIntent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> doAbout()
            R.id.menu_main_asc -> {
                mainViewModel.isAsc = true
                loadListBaseAdapter(this)
                "Sort By Ascending".showSnackbarAlert(this, main_layout)
            }
            R.id.menu_main_desc -> {
                mainViewModel.isAsc = false
                loadListBaseAdapter(this)
                "Sort By Descending".showSnackbarAlert(this, main_layout)
            }
            R.id.menu_main_reset_sort -> {
                mainViewModel.isAsc = null
                loadListBaseAdapter(this)
                "Reset sorting to Default List".showSnackbarAlert(this, main_layout)
            }
            R.id.menu_main_about -> doAbout()
            R.id.menu_main_logout -> doLogout()
        }
        return super.onOptionsItemSelected(item)
    }

    @SuppressLint("InflateParams")
    private fun doAbout() {
        val dialogView = LayoutInflater.from(this).inflate(R.layout.dialog_about, null)
        val builder = AlertDialog.Builder(this).setView(dialogView)
        val alertDialog = builder.show()
        val vivAboutCat = alertDialog.viv_about_cat
        vivAboutCat.setVideoURI(Uri.parse("android.resource://" + packageName + "/" + R.raw.cat))
        vivAboutCat.start()
        alertDialog.btn_about_close.setOnClickListener { alertDialog.dismiss() }
    }

    private fun doLogout() {
        val loginActivity = Intent(this, LoginActivity::class.java)
        startActivity(loginActivity)
        finish()
    }
}
