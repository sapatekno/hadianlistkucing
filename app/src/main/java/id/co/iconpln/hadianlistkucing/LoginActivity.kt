package id.co.iconpln.hadianlistkucing

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputEditText
import id.co.iconpln.hadianlistkucing.utils.editTextError
import id.co.iconpln.hadianlistkucing.utils.isEmailFormat
import id.co.iconpln.hadianlistkucing.utils.isValidPasswordLength
import id.co.iconpln.hadianlistkucing.utils.showSnackbarError
import kotlinx.android.synthetic.main.activity_login.*


class LoginActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        setButtonClickListener()
    }

    private fun setButtonClickListener() {
        btn_login_login.setOnClickListener { doLogin() }
    }

    private fun doLogin() {
        val error: Int = checkError(edt_login_password) + checkError(edt_login_email)
        if (error == 0) {
            checkLogin()
        }
    }

    private fun checkError(view: TextInputEditText): Int {
        return when {
            view.text.toString().isEmpty() -> "Value Is Empty".editTextError(view)
            else -> {
                when (view.id) {
                    R.id.edt_login_email -> {
                        return when {
                            !view.text.toString().isEmailFormat() -> "Not valid Email format".editTextError(
                                view
                            )
                            else -> 0
                        }
                    }
                    R.id.edt_login_password -> {
                        return when {
                            !view.text.toString().isValidPasswordLength() -> "Minimal Password length is 7".editTextError(
                                view
                            )
                            else -> 0
                        }
                    }
                    else -> 0
                }
            }
        }

    }

    private fun checkLogin() {
        closeSoftKeyboard()
        val email = edt_login_email.text.toString()
        val password = edt_login_password.text.toString()

        when {
            email == "user@mail.com" && password == "password" -> {
                val mainActivity = Intent(this, MainActivity::class.java)
                startActivity(mainActivity)
                finish()
            }
            else -> getString(R.string.login_error_text).showSnackbarError(this, login_layout)
        }
    }

    private fun closeSoftKeyboard() {
        try {
            val inputMethodManager =
                getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
        } catch (e: Exception) {
            // Dont do Anything
        }
    }
}